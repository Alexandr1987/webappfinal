create sequence hibernate_sequence start 1 increment 1;
create table t_recipe (
    id int8 not null,
    description TEXT,
    title varchar(255),
    user_id int8,
    primary key (id)
 );
create table t_user (
    user_id int8 not null,
    activation_code varchar(255),
    email varchar(255)not null,
    name varchar(255),
    password varchar(255)not null,
    surname varchar(255),
    telephone_number varchar(255),
      primary key (user_id)
  );
alter table t_recipe
    add constraint recipe_fk foreign key (user_id) references t_user;
