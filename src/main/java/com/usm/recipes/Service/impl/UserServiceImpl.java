package com.usm.recipes.Service.impl;

import com.usm.recipes.Service.UserService;
import com.usm.recipes.dao.UserDao;
import com.usm.recipes.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public void saveOrUpdateUser(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        userDao.save(user);
    }

    @Override
    public User getUsersByEmail(String email) {
        return userDao.findByEmail(email);
    }

    @Override
    public void deleteUser(Long id) {
        userDao.deleteById(id);
    }
}
