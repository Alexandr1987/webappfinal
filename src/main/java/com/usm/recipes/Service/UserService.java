package com.usm.recipes.Service;

import com.usm.recipes.model.User;

public interface UserService {

    void saveOrUpdateUser(User user);

    User getUsersByEmail(String email);
    void deleteUser(Long id);

}
