package com.usm.recipes.Service;

import com.usm.recipes.model.Recipe;
import com.usm.recipes.model.User;

import java.util.List;

public interface RecipeService {
    List<Recipe> getAllRecipes();

    List<Recipe> getAllRecipesByCreator(User creator);
   Recipe getRecipesById(Long id);
    void saveOrUpdate(Recipe recipe);
    void deleteRecipe(Long id);
}
