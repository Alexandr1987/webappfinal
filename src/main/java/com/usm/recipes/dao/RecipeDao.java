package com.usm.recipes.dao;

import com.usm.recipes.model.Recipe;
import com.usm.recipes.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RecipeDao extends JpaRepository<Recipe, Long> {
//@Query("SELECT recipe FROM Recipe recipe WHERE recipe.creator=:creator")
//   List<Recipe> getAllRecipesByCreator(@Param("creator") User creator);

   List<Recipe> getAllRecipesByCreator(User creator);
}
