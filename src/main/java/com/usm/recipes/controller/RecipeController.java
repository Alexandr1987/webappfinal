package com.usm.recipes.controller;

import com.usm.recipes.Service.RecipeService;
import com.usm.recipes.Service.UserService;
import com.usm.recipes.model.Recipe;
import com.usm.recipes.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.security.Principal;

@Controller
public class RecipeController {
@Autowired
    private RecipeService recipeService;
@Autowired
    private UserService userService;
    @Value("${upload.path}")
    private String uploadPath;
    @GetMapping("/create-recipe")
    public String enterRecipe(Model model){
        model.addAttribute("recipe",new Recipe());
        return "create-recipe";
    }

    @PostMapping("/create-recipe")
    public String registerRecipe(@ModelAttribute("recipe") Recipe recipe, Principal principal, Model model,@RequestParam("file") MultipartFile file
    ) throws IOException {
        User user = userService.getUsersByEmail(principal.getName());
        recipe.setCreator(user);

        if (file != null) {
            File upload = new File(uploadPath);

            if (!upload.exists()) {
                upload.mkdir();
            }
            file.transferTo(new File(uploadPath + "/"+ file.getOriginalFilename()));
            recipe.setFilename(file.getOriginalFilename());
        }

        recipeService.saveOrUpdate(recipe);
        model.addAttribute("user",user);
        model.addAttribute("recipes", recipeService.getAllRecipesByCreator(user));
        return "recipe";
    }

    @GetMapping("/view-recipes")
    public String showAllRecipes(@ModelAttribute("recipe") Recipe recipe, Principal principal, Model model){
        model.addAttribute("recipes", recipeService.getAllRecipes());
        return "view-recipes";
    }
}
