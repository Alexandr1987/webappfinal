package com.usm.recipes.controller;

import com.usm.recipes.Service.RecipeService;
import com.usm.recipes.model.User;
import com.usm.recipes.Service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.security.Principal;


@Controller
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private RecipeService recipeService;


    @GetMapping({"/","/login"})
    public String login(){
        return "login";
    }

    @GetMapping("/welcome")
    public String welcome(Principal principal, Model model) {
      User user =userService.getUsersByEmail(principal.getName());
      model.addAttribute("user", user);
      model.addAttribute("recipes", recipeService.getAllRecipesByCreator(user));
        return "recipe";
    }

    @GetMapping("/signup")
    public String signup(Model model){
        model.addAttribute("user",new User());
        return "register";
    }

    @PostMapping("/register")
    public String register(@ModelAttribute("user") User user, BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            return "register";
        }
        userService.saveOrUpdateUser(user);
        return "login";}


}

