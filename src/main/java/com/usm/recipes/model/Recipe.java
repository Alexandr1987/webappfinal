package com.usm.recipes.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "T_Recipe")
public class Recipe {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @Column( columnDefinition ="TEXT",name = "description")
    private String description;

    @Column(name = "title")
    private String title;

    @Column(name="file_name")
    private String filename;

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User creator;

    public Recipe(String description, String title, User creator) {
        this.description = description;
        this.title=title;
        this.creator = creator;
    }
    public Recipe(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle(){return title;}

    public void setTitle(String title){this.title=title;}

    public String getFilename() {return filename;}

    public void setFilename(String filename) {this.filename = filename;}

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Recipe recipe = (Recipe) o;
        return id.equals(recipe.id) &&
                Objects.equals(description, recipe.description) &&
                Objects.equals(title, recipe.title) &&
                creator.equals(recipe.creator);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, description, title, creator);
    }
}
