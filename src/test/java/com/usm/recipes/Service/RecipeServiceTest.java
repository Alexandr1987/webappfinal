package com.usm.recipes.Service;

import com.usm.recipes.Service.impl.RecipeServiceImpl;
import com.usm.recipes.dao.RecipeDao;
import com.usm.recipes.model.Recipe;
import com.usm.recipes.model.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class RecipeServiceTest {
    @Mock
    private RecipeDao recipeDao;
    @InjectMocks
    private RecipeService recipeService = new RecipeServiceImpl();;
    private User user;
    private Recipe recipe1;
    private Recipe recipe2;

    List<Recipe> recipeList;

    @BeforeEach
    void setUp() {
        user = new User("2@mail.ru","123456789","alex","bad","987654321");
        user.setId(1L);

        recipe1 = new Recipe("blah blah blah","cruton",user);
        recipe1.setId(1L);
        recipe2 = new Recipe("blah blah blah blah","cruton2",user);
        recipe2.setId(2L);
        recipeList = new ArrayList<>();
        recipeList.add(recipe1);
        recipeList.add(recipe2);
    }

    @AfterEach
    void tearDown() {

        verifyNoMoreInteractions(recipeDao);
    }

    @Test
    void getAllRecipes() {
        when(recipeDao.findAll()).thenReturn(recipeList);
        List<Recipe> resultRecipe = recipeService.getAllRecipes();
        verify(recipeDao, only()).findAll();

        assertEquals(recipeList, resultRecipe);
    }

    @Test
    void getAllRecipesByCreator() {
        when(recipeDao.getAllRecipesByCreator(any(User.class))).thenReturn(recipeList);
        List<Recipe> resultRecipe = recipeService.getAllRecipesByCreator(user);
        verify(recipeDao, only()).getAllRecipesByCreator(user);

        assertEquals(recipeList, resultRecipe);

    }

    @Test
    void getRecipesById() {
        when(recipeDao.findById(anyLong())).thenReturn(Optional.of(recipe1));
        Recipe resultRecipe = recipeService.getRecipesById(recipe1.getId());
        verify(recipeDao,only()).findById(recipe1.getId());
        assertEquals(recipe1,resultRecipe);
    }

    @Test
    void saveOrUpdate() {
        recipeService.saveOrUpdate(recipe1);
        verify(recipeDao, only()).save(recipe1);
    }

    @Test
    void deleteRecipe() {
        recipeService.deleteRecipe(recipe1.getId());
        verify(recipeDao, only()).deleteById(recipe1.getId());
    }
}