package com.usm.recipes.Service;

import com.usm.recipes.Service.impl.UserServiceImpl;
import com.usm.recipes.dao.UserDao;
import com.usm.recipes.model.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {
    @Mock
    private UserDao userDao;

    @Mock
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @InjectMocks
    UserService userService = new UserServiceImpl();

    User user;

    @BeforeEach
    void setUp() {
        user = new User("2@mail.ru","123456789","alex","bad","987654321");
        user.setId(1L);
    }

    @AfterEach
    void tearDown() {
        verifyNoMoreInteractions(userDao);
    }

    @Test
    void testSaveOrUpdateUser() {
        userService.saveOrUpdateUser(user);
        verify(userDao, only()).save(user);
    }

    @Test
    void testGetUsersByEmail() {
        when(userDao.findByEmail(anyString())).thenReturn(user);

        User resultUser = userService.getUsersByEmail("2@mail.ru");
        verify(userDao, only()).findByEmail("2@mail.ru");
        assertEquals(user, resultUser);
    }
}