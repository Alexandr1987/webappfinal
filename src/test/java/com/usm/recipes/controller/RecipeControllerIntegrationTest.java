package com.usm.recipes.controller;

import com.usm.recipes.Service.RecipeService;
import com.usm.recipes.Service.UserService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class RecipeControllerIntegrationTest {
    @Autowired
    private RecipeService recipeService;
    @Autowired
    private UserService userService;
    @Autowired
    MockMvc mockMvc;
    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }
    @WithMockUser
    @Test
    void enterRecipe() throws Exception {
        this.mockMvc.perform(get("/create-recipe"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void registerRecipe() {
    }
    @WithMockUser
    @Test
    void showAllRecipes() throws Exception {
        this.mockMvc.perform(get("/view-recipes"))
                .andDo(print())
                .andExpect(status().isOk());
    }
}