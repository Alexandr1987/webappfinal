package com.usm.recipes.controller;

import com.usm.recipes.Service.RecipeService;
import com.usm.recipes.Service.UserService;
import com.usm.recipes.model.Recipe;
import com.usm.recipes.model.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import org.springframework.http.MediaType;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
class UserControllerIntegrationTest {

    @Autowired
    private UserService userService;
    @Autowired
    private RecipeService recipeService;
    @Autowired
    MockMvc mockMvc;
    private User user;
    private Recipe recipe1;
    private Recipe recipe2;

    List<Recipe> recipeList;

    @BeforeEach
    void setUp() {
        user = new User("gamma@mail.ru","123456789","alex","bad","987654321");
        user.setId(999L);
        recipe1 = new Recipe("blah blah blah","cruton",user);
        recipe1.setId(1L);
        recipe2 = new Recipe("blah blah blah blah","cruton2",user);
        recipe2.setId(2L);
        recipeList = new ArrayList<>();
        recipeList.add(recipe1);
        recipeList.add(recipe2);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void login() throws Exception {
        this.mockMvc.perform(formLogin().user("email","smoky1987@mail.ru").password("123456"))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/welcome"));

        this.mockMvc.perform(formLogin().user("email","smoky19387@mail.ru").password("1223absd"))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/login?error"));
    }

//    @WithMockUser
    @Test
    void welcome() throws Exception{
        this.mockMvc.perform(formLogin().user("email", "smoky1987@mail.ru").password("123456"))
                .andDo(print())
                .andExpect(redirectedUrl("/welcome"));
    }

    @Test
    void signup() throws Exception{
        this.mockMvc.perform(get("/signup"))
            .andDo(print())
            .andExpect(status().isOk());
    }

    @Test
    void register() throws Exception{
        mockMvc.perform(post("/register")
                .flashAttr("user", user))
                .andDo(print())
                .andExpect(status().isOk());
        User existingUser = userService.getUsersByEmail(user.getEmail());
        if (existingUser != null) {
            userService.deleteUser(existingUser.getId());
        }
    }
}